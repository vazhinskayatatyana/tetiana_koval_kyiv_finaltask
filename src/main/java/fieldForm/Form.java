package fieldForm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pages.BasePage;

import java.util.HashMap;

public class Form extends BasePage {

    private static final long DEFAULT_TIMEOUT = 60;

    public Form(WebDriver driver) {
        super(driver);
    }

    public void fillForm(HashMap<String, String> form) {
        var template = "//*[contains(@aria-label, '%s')]";
        for (var entry : form.entrySet()) {
            var locator = By.xpath(template.formatted(entry.getKey()));
            waitVisibilityElementByLocator(DEFAULT_TIMEOUT, locator);
            var elem = driver.findElement(locator);
            elem.clear();
            elem.sendKeys(entry.getValue());
        }
    }
}
