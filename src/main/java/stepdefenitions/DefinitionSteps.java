package stepdefenitions;

import fieldForm.Form;
import scoreBoard.ScoreBoard;
import scoreBoard.Score;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import java.util.HashMap;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    WebDriver driver;
    PageFactoryManager pageFactoryManager;
    HomePage homePage;
    SearchResultPage searchResultPage;
    FootballPage footballPage;
    IframePage iframePage;
    NewsPage newsPage;
    ScoresAndFixturesPage scoresAndFixturesPage;
    SearchPage searchPage;
    SportPage sportPage;
    ScoreBoard scoreBoard;
    CoronavirusPage coronavirusPage;
    QuestionsPage questionsPage;
    Form form;

    private static final long DEFAULT_TIMEOUT = 60;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @After
    public void tearDown() {
        driver.close();
    }

    @Given("User opens {string} page")
    public void openPage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openHomePage(url);
    }


    @When("User goes to the news page")
    public void goToTheNewsPage() {
        homePage.clickNewsPageButton();
    }


    @And("User closes registration popup")
    public void closeRegistrationPopup() {
        iframePage = pageFactoryManager.getIframePage();
        iframePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, iframePage.getRegistrationPopUp());
        iframePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, iframePage.getCloseRegistrationPopUp());
        iframePage.clickCloseRegistrationPopUp();
    }

    @Then("User checks the name of main article by {string}")
    public void checkTheNameOfMainArticle(final String headline) {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(newsPage.getMainArticleText().contains(headline));
    }


    @Then("User checks the names of the secondary articles by {string}")
    public void checksTheNamesOfTheSecondaryArticles(final String headline) {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, newsPage.getBelowArticle());
        var headlineFound = false;
        for (var element : newsPage.getBelowArticles()) {
            if (element.getText().equals(headline)) {
                headlineFound = true;
                break;
            }
        }
        assertTrue(headlineFound);
    }

    @When("User makes search by stores text")
    public void searchByStoresText() {
        newsPage = pageFactoryManager.getNewsPage();
        var text = newsPage.getMainArticleText();
        newsPage.clickSearchButton();
        searchPage = pageFactoryManager.getSearchPage();
        searchPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchPage.enterTextToSearchField(text);
        searchPage.clickSearchButton();
    }

    @Then("User checks that name of the first article in the search results against a specified value {string}")
    public void checkThatNameOfTheFirstArticle(final String headline) {
        searchResultPage = pageFactoryManager.getSearchResultPage();
        searchResultPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(searchResultPage.getHeadlineOfTheFirstArticleText()
                .contains(headline));
    }

    @And("User goes to the sport page")
    public void goToTheSportPage() {
        homePage = pageFactoryManager.getHomePage();
        homePage.clickSportPageButton();
    }

    @And("User goes to the football page")
    public void goToTheFootballPage() {
        sportPage = pageFactoryManager.getSportPage();
        sportPage.clickFootballPageButton();
    }


    @And("User goes to the scores and fixtures page")
    public void goesToTheScoresAndFixturesPage() {
        footballPage = pageFactoryManager.getFootballPage();
        footballPage.clickScoresAndFixturesPageButton();
    }

    @And("User search the championship by {string}")
    public void searchTheChampionship(final String championship) {
        scoresAndFixturesPage = pageFactoryManager.getScoresAndFixturesPage();
        scoresAndFixturesPage.enterTextToSearchField(championship);
        scoresAndFixturesPage.clickChooseTheSearchResult();
    }

    @And("User selects the month by {string}")
    public void selectsTheMonth(final String month) {
        scoresAndFixturesPage = pageFactoryManager.getScoresAndFixturesPage();
        scoresAndFixturesPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        scoresAndFixturesPage.clickChooseMonth(month);
    }

    @Then("User checks the scores of two teams{string},{string},{string}, {string}")
    public void checkTheScoresOfTeams(final String homeTeam, final String awayTeam,
                                      final String homeScore, final String awayScore) {
        scoresAndFixturesPage = pageFactoryManager.getScoresAndFixturesPage();
        scoresAndFixturesPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        scoresAndFixturesPage.waitVisibilityOfElements(DEFAULT_TIMEOUT, scoresAndFixturesPage.getChooseTeemsScore());
        scoreBoard = pageFactoryManager.getScoreBoardPage();
        assertEquals(scoreBoard.getScore(homeTeam, awayTeam), new Score(homeScore, awayScore));
    }

    @And("User goes to team result by {string}")
    public void goToTeamResult(final String firstTeam) {
        scoresAndFixturesPage = pageFactoryManager.getScoresAndFixturesPage();
        scoresAndFixturesPage.clickChooseTeam(firstTeam);
    }

    @And("User goes to the coronavirus page")
    public void goToTheCoronavirusPage() {
        newsPage = pageFactoryManager.getNewsPage();
        newsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        newsPage.clickCoronavirusButton();
    }

    @When("User selects your coronavirus stories and goes to questions page")
    public void selectYourCoronavirusStories() {
        coronavirusPage = pageFactoryManager.getCoronavirusPage();
        coronavirusPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        coronavirusPage.clickCoronavirusStoriesButton();
        coronavirusPage.clickQuestionPageButton();
    }

    @And("User submit question form")
    public void submitQuestionForm() {
        questionsPage = pageFactoryManager.getQuestionsPage();
        questionsPage.clickSubmitButton();
    }

    @Then("User sees Error messages by {string}")
    public void userSeesErrorMessagesByError(final String error) {
        questionsPage = pageFactoryManager.getQuestionsPage();
        questionsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        questionsPage.waitVisibilityOfElements(DEFAULT_TIMEOUT, questionsPage.getErrorMessages());
        assertEquals(questionsPage.getErrorMessagesText(error), error);
    }

    @And("User fills question field by {string}")
    public void fillQuestionField(final String question) {
        form = pageFactoryManager.getForm();
        form.fillForm(new HashMap<>() {{
            put("What questions would you like us to answer?", question);
        }});
    }

    @And("User fills name field by {string}")
    public void fillNameField(final String name) {
        form = pageFactoryManager.getForm();
        form.fillForm(new HashMap<>() {{
            put("Name", name);
        }});
    }

    @And("User fills email field by {string}")
    public void fillEmailField(final String email) {
        form = pageFactoryManager.getForm();
        form.fillForm(new HashMap<>() {{
            put("Email address", email);
        }});
    }


    @And("User accepts the Terms")
    public void acceptTheTerms() {
        questionsPage = pageFactoryManager.getQuestionsPage();
        questionsPage.clickAcceptingTermsButton();
    }
}
