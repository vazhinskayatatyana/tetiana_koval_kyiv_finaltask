package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class QuestionsPage extends BasePage {

    @FindBy(xpath = "//div[@class='button-container']")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@class='input-error-message']")
    private List<WebElement> errorMessages;

    @FindBy(xpath = "//input[@type='checkbox']")
    private WebElement acceptingTermsButton;

    public void clickSubmitButton() {
        submitButton.click();
    }

    public List<WebElement> getErrorMessages(){return errorMessages;}

    public String getErrorMessagesText(String error) {
        for (var element : errorMessages) {
            if (error.equals(element.getText())) {
                return error;
            }
        }
        return null;
    }

    public void clickAcceptingTermsButton(){
        acceptingTermsButton.click();
    }

    public QuestionsPage(WebDriver driver) {
        super(driver);
    }
}
