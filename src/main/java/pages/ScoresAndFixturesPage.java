package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ScoresAndFixturesPage extends BasePage {

    private static final String NAME_HOME_TEAM =
            "//span[@class='sp-c-fixture__team-name sp-c-fixture__team-name--home']";

    @FindBy(xpath = "//input[@id='downshift-0-input']")
    private WebElement searchField;

    @FindBy(xpath = "//a[@id='downshift-0-item-0']")
    private WebElement chooseTheSearchResult;

    @FindBy(xpath = "//span[contains(@class,'gel-long-primer-bold gs-u-display-block')]")
    private List<WebElement> chooseMonth;

    @FindBy(xpath = "//li[@class='gs-o-list-ui__item gs-u-pb-']")
    private List<WebElement> chooseTeemsScore;


    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickChooseTheSearchResult() {
        chooseTheSearchResult.click();
    }

    public void clickChooseMonth(final String month) {
        for (var element : chooseMonth) {
            if (element.getText().equals(month)) {
                element.click();
                break;
            }
        }
    }

    public List<WebElement> getChooseTeemsScore(){return chooseTeemsScore;}

    public void clickChooseTeam(final String homeTeam) {

        for (var element : chooseTeemsScore) {
            if (element.findElement(By.xpath(NAME_HOME_TEAM)).getText().equals(homeTeam)
            ) {
                element.click();
                break;
            }
        }
    }

    public ScoresAndFixturesPage(WebDriver driver) {
        super(driver);
    }
}
