package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IframePage extends BasePage {

    @FindBy(xpath = "//button[@class='tp-close tp-active']")
    private WebElement closeRegistrationPopUp;

    @FindBy(xpath = "//div[@class='tp-iframe-wrapper tp-active']")
    private WebElement registrationPopUp;


    public WebElement getRegistrationPopUp() {
        return registrationPopUp;
    }

    public WebElement getCloseRegistrationPopUp() {
        return closeRegistrationPopUp;
    }

    public void clickCloseRegistrationPopUp() {
        closeRegistrationPopUp.click();
    }

    public IframePage(WebDriver driver) {
        super(driver);
    }
}
