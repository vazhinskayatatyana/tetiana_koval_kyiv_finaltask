package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class FootballPage extends BasePage {

    @FindBy(xpath = "//a[@data-stat-name='secondary-nav-v2']")
    private List<WebElement> scoresAndFixturesPageButton;

    public void clickScoresAndFixturesPageButton(){
        scoresAndFixturesPageButton.get(1).click();
    }
    public FootballPage(WebDriver driver) {
        super(driver);
    }
}
