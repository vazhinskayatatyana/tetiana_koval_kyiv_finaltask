package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultPage extends BasePage{

    @FindBy(xpath = "//a[@class='ssrcss-1ynlzyd-PromoLink e1f5wbog0']")
    private List<WebElement> headlineOfTheFirstArticle;

    public String getHeadlineOfTheFirstArticleText(){return headlineOfTheFirstArticle.get(0).getText();}
    public SearchResultPage(WebDriver driver) {
        super(driver);
    }
}
