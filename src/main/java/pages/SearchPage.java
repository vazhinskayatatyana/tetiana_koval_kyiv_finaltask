package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage extends BasePage{

    @FindBy(xpath = "//input[@data-testid='test-search-input']")
    private WebElement searchField;

    @FindBy(xpath = "//button[@data-testid='test-search-submit']")
    private WebElement searchButton;

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton(){
        searchButton.click();
    }
    public SearchPage(WebDriver driver) {
        super(driver);
    }
}
