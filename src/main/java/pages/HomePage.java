package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage{

    @FindBy(xpath = "//li[@class='orb-nav-newsdotcom']")
    private List<WebElement> newsPageButton;

    @FindBy(xpath = "//li[@class='orb-nav-sport']")
    private List<WebElement> sportPageButton;

    public void clickNewsPageButton(){
        newsPageButton.get(0).click();
    }

    public void clickSportPageButton(){
        sportPageButton.get(1).click();
    }

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }
}
