package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class NewsPage extends BasePage{

    @FindBy(xpath = "//h3[@class='gs-c-promo-heading__title gel-paragon-bold nw-o-link-split__text']")
    private WebElement mainArticle;

    @FindBy(xpath = "//a[contains(@class,'gs-c-promo-heading gs-o-faux-block-link__overlay-link gel-p')]")
    private List<WebElement> belowArticles;

    @FindBy(xpath = "//a[@class='orbit-search__button']")
    private WebElement searchButton;

    @FindBy(xpath="//a[@class='nw-o-link']")
    private List<WebElement> coronavirusButton;

    public String getMainArticleText(){return mainArticle.getText();}

    public List<WebElement> getBelowArticles(){return belowArticles;}

    public WebElement getBelowArticle(){return belowArticles.get(2);}

    public void clickSearchButton(){searchButton.click();}

    public void clickCoronavirusButton(){coronavirusButton.get(2).click();}

    public NewsPage(WebDriver driver) {
        super(driver);
    }
}
