package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SportPage extends BasePage {

    @FindBy(xpath = "//span[@class='ssrcss-1yycgk3-LinkTextContainer eis6szr1']")
    private List<WebElement> footballPageButton;

    public void clickFootballPageButton(){
        footballPageButton.get(1).click();
    }

    public SportPage(WebDriver driver) {
        super(driver);
    }
}
