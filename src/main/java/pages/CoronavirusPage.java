package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CoronavirusPage extends BasePage{

    @FindBy(xpath="//li[@class='gs-o-list-ui__item--flush gel-long-primer gs-u-display-block gs-u-float-left nw-c-nav__secondary-menuitem-container']")
    private WebElement coronavirusStoriesButton;

    @FindBy(xpath="//a[@class='gs-c-promo-heading gs-o-faux-block-link__overlay-link gel-pica-bold nw-o-link-split__anchor']")
    private List<WebElement> questionPageButton;

    public void clickCoronavirusStoriesButton(){coronavirusStoriesButton.click();}

    public WebElement getCoronavirusStoriesButton (){
        return coronavirusStoriesButton;
    }

    public void clickQuestionPageButton(){questionPageButton.get(19).click();}

    public CoronavirusPage(WebDriver driver) {
        super(driver);
    }
}
