package scoreBoard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;

import java.util.List;

public class ScoreBoard extends BasePage {

    private static final String NAME_HOME_TEAM =
            "//span[@class='sp-c-fixture__team-name sp-c-fixture__team-name--home']";
    private static final String NAME_AWAY_TEAM =
            "//span[@class='sp-c-fixture__team-name sp-c-fixture__team-name--away']";
    private static final String SCORE_HOME_TEAM =
            "//span[@class='sp-c-fixture__number sp-c-fixture__number--home sp-c-fixture__number--ft']";
    private static final String SCORE_AWAY_TEAM =
            "//span[@class='sp-c-fixture__number sp-c-fixture__number--away sp-c-fixture__number--ft']";

    @FindBy(xpath = "//li[@class='gs-o-list-ui__item gs-u-pb-']")
    private List<WebElement> chooseTeemsScore;

    public Score getScore(final String homeTeam, final String awayTeam) {

        for (var element : chooseTeemsScore) {
            if (element.findElement(By.xpath(NAME_HOME_TEAM)).getText().equals(homeTeam)
                    && element.findElement(By.xpath(NAME_AWAY_TEAM)).getText().equals(awayTeam)
            ) {
                return new Score(
                        element.findElement(By.xpath(SCORE_HOME_TEAM)).getText(),
                        element.findElement(By.xpath(SCORE_AWAY_TEAM)).getText()
                );
            }
        }

        return null;
    }

    public ScoreBoard(WebDriver driver) {
        super(driver);
    }
}
