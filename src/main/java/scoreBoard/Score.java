package scoreBoard;

public class Score {

    public final String homeScore;
    public final String awayScore;

    public Score(String homeScore, String awayScore) {
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof Score s)) {
            return false;
        }

        return awayScore.equals(s.awayScore) && homeScore.equals(s.homeScore);
    }
}
