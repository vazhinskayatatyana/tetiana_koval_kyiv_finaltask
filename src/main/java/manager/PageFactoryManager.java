package manager;

import fieldForm.Form;
import scoreBoard.ScoreBoard;
import org.openqa.selenium.WebDriver;
import pages.*;


public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage () {
        return new HomePage(driver);
    }

    public NewsPage getNewsPage (){return new NewsPage(driver);}

    public IframePage getIframePage(){return new IframePage(driver);}

    public SearchPage getSearchPage(){return new SearchPage(driver);}

    public SearchResultPage getSearchResultPage(){return new SearchResultPage(driver);}

    public FootballPage getFootballPage () {
        return new FootballPage(driver);
    }

    public SportPage getSportPage (){return new SportPage(driver);}

    public ScoresAndFixturesPage getScoresAndFixturesPage(){return new ScoresAndFixturesPage(driver);}

    public  ScoreBoard getScoreBoardPage(){return new ScoreBoard(driver);}

    public CoronavirusPage getCoronavirusPage(){return new CoronavirusPage(driver);}

    public QuestionsPage getQuestionsPage(){return new QuestionsPage(driver);}

    public Form getForm(){return new Form(driver);}

}
