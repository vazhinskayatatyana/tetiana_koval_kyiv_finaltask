Feature: Smoke tests
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check the name of the main article

    Given User opens '<homePage>' page
    When User goes to the news page
    And User closes registration popup
    Then User checks the name of main article by '<headline>'
    Examples:
      | homePage                    |headline                                           |
      | https://www.bbc.com/        | Donbas region completely destroyed, Zelensky says |

  Scenario Outline: Check the names of the secondary articles

    Given User opens '<homePage>' page
    When User goes to the news page
    And User closes registration popup
    Then User checks the names of the secondary articles by '<headline>'

    Examples:
      | homePage               | headline                                            |
      | https://www.bbc.com/   | US fully backs Sweden and Finland Nato bids - Biden |
      | https://www.bbc.com/   | North Korea fighting Covid with tea and salt water  |
      | https://www.bbc.com/   | Monkeypox investigated in Europe, US, Australia     |
      | https://www.bbc.com/   | UK report planning to name Downing St rule-breakers |

  Scenario Outline: Check search the article
    Given User opens '<homePage>' page
    And User goes to the news page
    And User closes registration popup
    When User makes search by stores text
    Then User checks that name of the first article in the search results against a specified value '<article headline>'

    Examples:
      | homePage                    | article headline                                              |
      | https://www.bbc.com/        | Russia troops advancing in Luhansk as east Ukraine under fire |

  Scenario Outline: Check the team scores
    Given User opens '<homePage>' page
    And User goes to the sport page
    And User goes to the football page
    And User goes to the scores and fixtures page
    And User search the championship by '<search text>'
    And User selects the month by '<month>'
    Then User checks the scores of two teams'<home team>','<away team>','<home score>', '<away score>'
    And User goes to team result by '<home team>'
    Then User checks the scores of two teams'<home team>','<away team>','<home score>', '<away score>'

    Examples:
      | homePage                    | month       | home team        | away team               | home score  | away score  | search text           |
      | https://www.bbc.com/        | APR         | Arbroath         | Greenock Morton         | 3           | 0           | Scottish Championship |
      | https://www.bbc.com/        | APR         | Blackburn Ladies | Charlton Athletic Women | 0           | 3           | Women\'s Championship |
      | https://www.bbc.com/        | MAR         | Juventus         | Villarreal              | 0           | 3           | Champions League      |
      | https://www.bbc.com/        | FEB         | Liverpool        | Cardiff City            | 3           | 1           | FA Cup                |