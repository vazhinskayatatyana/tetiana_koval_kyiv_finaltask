Feature: Smoke tests of the field form
  As a user
  I want to test field form
  So that I can be sure that functionality works correctly

  Scenario Outline: Check that the question, name fields are required in the form

    Given User opens '<homePage>' page
    And User goes to the news page
    And User closes registration popup
    And User goes to the coronavirus page
    When User selects your coronavirus stories and goes to questions page
    And User submit question form
    Then User sees Error messages by '<error>'

    Examples:
      | homePage                    | error                           |
      | https://www.bbc.com/        |  can\'t be blank                |
      | https://www.bbc.com/        |  Name can\'t be blank           |

  Scenario Outline: Check that the accept the terms is required in the form

    Given User opens '<homePage>' page
    And User goes to the news page
    And User closes registration popup
    And User goes to the coronavirus page
    When User selects your coronavirus stories and goes to questions page
    And User fills question field by '<question>'
    And User fills name field by '<name>'
    And User fills email field by '<email>'
    And User submit question form
    Then User sees Error messages by '<error>'

    Examples:
      | homePage                    | error                                 |name     | question | email|
      | https://www.bbc.com/        | must be accepted                      |Tanya    | 123      | 123  |

  Scenario Outline: Check that the "Email" field is required in the form

    Given User opens '<homePage>' page
    And User goes to the news page
    And User closes registration popup
    And User goes to the coronavirus page
    When User selects your coronavirus stories and goes to questions page
    And User fills question field by '<question>'
    And User fills name field by '<name>'
    And User accepts the Terms
    And User submit question form
    Then User sees Error messages by '<error>'

    Examples:
      | homePage                    | error                                 | name      | question |
      | https://www.bbc.com/        | Email address can\'t be blank          | Tanya    | 123      |