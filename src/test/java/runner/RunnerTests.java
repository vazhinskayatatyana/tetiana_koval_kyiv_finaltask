package runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

    @RunWith(Cucumber.class)
    @CucumberOptions(
            features = {"src/main/resources/smoke tests BBC2.feature", "src/main/resources/smoke tests BBC1.feature"},
            glue = "stepdefenitions"
    )
    public class RunnerTests {
    }

